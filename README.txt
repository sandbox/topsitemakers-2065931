
This module provides Rules implementation for the core Poll module. It will
add two new actions to the Rules screen: when user casts a vote and when user
cancels a vote.

Usage
-----
1. Place this module in `sites/all/modules` (or any other module directory).
2. Enable it from admin or CLI using drush[0].
3. Create a new rule with custom actions.

[0] http://www.drush.org/

Author
------
Topsitemakers - https://drupal.org/user/2306644
www.topsitemakers.com
