<?php

/**
 * @file
 * Implementation of rules hooks and actions.
 *
 * Created by: Topsitemakers
 * http://www.topsitemakers.com/
 */

/**
 * Implements hook_rules_event_info().
 */
function poll_rules_rules_event_info() {
  return array(
    'poll_rules_poll_vote' => array(
      'label' => t('User casts a poll vote'),
      'help' => t('Triggers when users are casting votes on polls.'),
      'group' => t('Poll'),
      'variables' => array(
        'poll' => array(
          'type' => 'node',
          'label' => t('Poll node'),
          'description' => t('Voted poll node.'),
        ),
      ),
    ),
    'poll_rules_poll_cancel' => array(
      'label' => t('User cancels a poll vote'),
      'help' => t('Triggers when user cancels poll vote.'),
      'group' => t('Poll'),
      'variables' => array(
        'poll' => array(
          'type' => 'node',
          'label' => t('Poll node'),
          'description' => t('Voted poll node.'),
        ),
      ),
    ),
  );
}
